from flask import Flask, render_template, request
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer


import os
import logging
logger = logging.getLogger()
logger.setLevel(logging.ERROR)


bot = ChatBot('ARI',storage_adapter='chatterbot.storage.SQLStorageAdapter',
    logic_adapters=[
        {
            'import_path': 'chatterbot.logic.BestMatch',
            'default_response': 'I am sorry, but I do not understand.',
            'maximum_similarity_threshold': 0.70
        }
    ]) #create the bot

Aribot = ListTrainer(bot) # Teacher


for convo in os.listdir('base'):
	BotMemory = open('base/'+ convo, 'r').readlines()
	Aribot.train(BotMemory)



app = Flask(__name__)

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/get')
def get_bot_response():
    userText = request.args.get('msg')
    return str(bot.get_response(userText))


if __name__=='__main__':
	app.run(debug=True,port=5006)